package com.proyecto1.usuario.dto;

import lombok.Data;

@Data
public class UsuarioResponse {
	private String nombre;
	private String apellido;
	private int numeroTelefono;
	private String direccion;
	private String correo;
}
