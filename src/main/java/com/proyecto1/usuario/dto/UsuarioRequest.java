package com.proyecto1.usuario.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@ApiModel(description = "Representa los usuarios")
public class UsuarioRequest {
	@ApiModelProperty(notes = "nombre del usuario", example = "julio", required = true)
	@NotEmpty(message = "ingresar un nombre  ")
	private String nombre;
	@ApiModelProperty(notes = "apellido del usuario", example = "martinez", required = true)
	@NotEmpty(message = "ingresar un apellido ")
	private String apellido;
	@ApiModelProperty(notes = "numero de telefono del usuario", example = "123455", required = true)
	@Min(value = 1 ,message = "ingresar un numero valido")
	private int numeroTelefono;
	@ApiModelProperty(notes = "direccion del usuario", example = "av 12 # 78-90", required = true)
	@NotEmpty(message = "ingresar una direccion")
	private String direccion;
	@ApiModelProperty(notes = "correo del usuario", example = "juliomartinez@gmail.com", required = true)
	@NotEmpty(message = "ingresar un correo")
	@Email(message = "ingresar un correo valido")
	private String correo;
	@ApiModelProperty(notes = "contraseña del usuario", example = "julio1324", required = true)
	@NotEmpty(message = "ingresar una contrasena")
	private String contrasena;
}
