package com.proyecto1.usuario.util;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.proyecto1.usuario.dto.UsuarioResponse;
import com.proyecto1.usuario.entities.Usuario;

@Component
public class EntidadToConverter {
	@Autowired
	private ModelMapper modelMapper;
	
	public UsuarioResponse convertirEntidad(Usuario usuario) {
		return modelMapper.map(usuario, UsuarioResponse.class);
	}
	
	public List<UsuarioResponse> convertirEntidad (List<Usuario> usuarios){
		return usuarios.stream().map(usuario -> convertirEntidad(usuario)).collect(Collectors.toList());
	}
}
