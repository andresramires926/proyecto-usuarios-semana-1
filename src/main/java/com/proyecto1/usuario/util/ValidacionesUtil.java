package com.proyecto1.usuario.util;

public class ValidacionesUtil {
	
	private static String minusculas ="abcdefghyjklmnñopqrstuvwxyz";
	private static String mayusculas="ABCDEFGHYJKLMNÑOPQRSTUVWXYZ";

	public static Boolean isContrasenaValida(String contrasena) {
		Boolean isValida = false;
		char[] caracter = { '*', '_', '-' };
		for (int i = 0; i < contrasena.length(); i++) {
			char chr = contrasena.charAt(i);
			for (int j = 0; j < caracter.length; j++) {
				if (caracter[j] == chr) {
					if(isContieneMayusculas(contrasena) && isContieneMinusculas(contrasena)) {
						isValida = true;
					}
					
				} 
			}
		}
		
		return isValida;
	}
	
	public static Boolean isContieneMinusculas(String contrasena){
		Boolean isValida = false;
		for(int i=0; i<contrasena.length(); i++){
		      if (minusculas.indexOf(contrasena.charAt(i),0)!=-1){
		         isValida = true;
		      }
		   }
		return isValida;
	}
	
	public static Boolean isContieneMayusculas(String contrasena){
		Boolean isValida = false;
		for(int i=0; i<contrasena.length(); i++){
		      if (mayusculas.indexOf(contrasena.charAt(i),0)!=-1){
		         isValida = true;
		      }
		   }
		return isValida;
	}

}
