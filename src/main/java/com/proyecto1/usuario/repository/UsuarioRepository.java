package com.proyecto1.usuario.repository;


import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.proyecto1.usuario.entities.Usuario;
@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
		public List<Usuario> findByNombre(String nombre);
}
