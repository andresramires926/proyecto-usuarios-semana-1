package com.proyecto1.usuario.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto1.usuario.dto.UsuarioRequest;
import com.proyecto1.usuario.entities.Usuario;
import com.proyecto1.usuario.repository.UsuarioRepository;
import com.proyecto1.usuario.util.ValidacionesUtil;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	public Usuario registrarUsuario(UsuarioRequest usuarioNuevo) {
		Usuario usuario = new Usuario();

		usuario.setNombre(usuarioNuevo.getNombre());
		usuario.setApellido(usuarioNuevo.getApellido());
		usuario.setDireccion(usuarioNuevo.getDireccion());
		usuario.setNumeroTelefono(usuarioNuevo.getNumeroTelefono());

		if (usuarioNuevo.getContrasena().length() < 15 && usuarioNuevo.getContrasena().length() > 7
				&& ValidacionesUtil.isContrasenaValida(usuarioNuevo.getContrasena())) {

			usuario.setContrasena(usuarioNuevo.getContrasena());

		} else {
			usuario.setContrasena(null);
		}
		usuario.setCorreo(usuarioNuevo.getCorreo());
		usuarioRepository.save(usuario);

		return usuario;
	}

	public List<Usuario> listarUsuarios() {
		List<Usuario> listaUsuarios = usuarioRepository.findAll();
		return listaUsuarios;
	}

	public List<Usuario> buscarPorNombre(String nombre) {
		List<Usuario> listaUsuario = usuarioRepository.findByNombre(nombre);
		return listaUsuario;
	}

	public Usuario buscarPorId(Long id) {
		return usuarioRepository.findById(id).orElse(null);
	}
}
