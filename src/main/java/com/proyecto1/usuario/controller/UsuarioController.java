package com.proyecto1.usuario.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto1.usuario.dto.UsuarioRequest;
import com.proyecto1.usuario.dto.UsuarioResponse;
import com.proyecto1.usuario.entities.Usuario;
import com.proyecto1.usuario.service.UsuarioService;
import com.proyecto1.usuario.util.EntidadToConverter;

@RestController

public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private EntidadToConverter convertidor;
	
	
	@PostMapping( value = "registro")
	public ResponseEntity<UsuarioResponse> crearUsuario (@Validated @RequestBody UsuarioRequest payload){
		Usuario usuario = usuarioService.registrarUsuario(payload);
		return new ResponseEntity<>(convertidor.convertirEntidad(usuario), HttpStatus.CREATED);
	}
	
	@GetMapping( value = "listaUsuario")
	public ResponseEntity<List<UsuarioResponse>> listarUsuarios(){
		List<Usuario> listaUsuarios = usuarioService.listarUsuarios();
		return new ResponseEntity<>(convertidor.convertirEntidad(listaUsuarios), HttpStatus.OK);
	}
	
	@GetMapping(value = "buscar/nombre/{nombre}")
	public ResponseEntity<List<UsuarioResponse>> buscarPorNombre(@PathVariable String nombre){
		List<Usuario> listaUsuario = usuarioService.buscarPorNombre(nombre);
		return new ResponseEntity<>(convertidor.convertirEntidad(listaUsuario), HttpStatus.ACCEPTED);
	}
	
	@GetMapping(value = "buscar/id/{id}")
	public ResponseEntity<UsuarioResponse> buscarPorId(@PathVariable Long id){
		Usuario Usuario = usuarioService.buscarPorId(id);
		return new ResponseEntity<>(convertidor.convertirEntidad(Usuario), HttpStatus.ACCEPTED);
	}
}
