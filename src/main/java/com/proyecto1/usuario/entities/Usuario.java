package com.proyecto1.usuario.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tbl_usuario")
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "nombre_usuario", length = 50)
	private String nombre;
	@Column(name = "apellido_usuario", length = 50)
	private String apellido;
	@Column(name = "numero_usuario", length = 30)
	private int numeroTelefono;
	@Column(name = "direccion_usuario", length = 30)
	private String direccion;
	@Column(name = "correo_usuario", length = 50)
	private String correo;
	@Column (name = "contrasena", length = 30)
	@NotNull
	private String contrasena;
}
