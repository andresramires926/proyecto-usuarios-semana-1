package com.proyecto1.practica;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.proyecto1.usuario.dto.UsuarioRequest;
import com.proyecto1.usuario.entities.Usuario;
import com.proyecto1.usuario.repository.UsuarioRepository;
import com.proyecto1.usuario.service.UsuarioService;

@SpringBootTest(classes = UsuarioRepository.class)
@ExtendWith(SpringExtension.class)
class UsuarioServiceTests {

	@InjectMocks
	UsuarioService usuarioService;

	@Mock
	UsuarioRepository usuarioRepository;

	@Test
	public void testDebeRegistrarUsuario() {

		// Given
		Usuario usuarioResponse = new Usuario();
		usuarioResponse.setId(1L);
		usuarioResponse.setNombre("julio");
		usuarioResponse.setApellido("Martinez");
		usuarioResponse.setDireccion("av siempre viva");
		usuarioResponse.setNumeroTelefono(311210878);
		usuarioResponse.setContrasena("qwe1232");
		usuarioResponse.setCorreo("juliomartinez@gmail.com");

		UsuarioRequest usuarioRequest = new UsuarioRequest();
		usuarioRequest.setNombre("julio");
		usuarioRequest.setApellido("Martinez");
		usuarioRequest.setDireccion("av siempre viva");
		usuarioRequest.setNumeroTelefono(311210878);
		usuarioRequest.setContrasena("qwe1232");
		usuarioRequest.setCorreo("juliomartinez@gmail.com");

		// When
		when(usuarioRepository.save(any())).thenReturn(usuarioResponse);

		usuarioService.registrarUsuario(usuarioRequest);

		// Then
		assertEquals(usuarioResponse.getNombre(), usuarioRequest.getNombre());
		 //verify(usuarioRepository.save(any(Usuario.class)));
	}
	
	@Test
	public void testDebeListarUsuario() {
		 
	}

}
